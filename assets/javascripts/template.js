require("../stylesheets/template.scss");

/**
 *  Main javascript file
 *
 *  Because we defined an environment object in our base.phtml, we can access some usefull information now:
 *
 *  window.environment.getCurrentPage()
 *  to get the current pages name like "contact.html"
 *
 *  window.environment.getParentPage()
 *  to get the parent pages name like "about.html"
 *
 *  window.environment.getMainURL()
 *  to get the main url like "https://www.your-nice-page.com"
 *
 *  window.environment.getLanguageCode()
 *  to get the current language code like "de" for German
 *
 *  window.environment.getCountryCode()
 *  to get the current country code like "ch" for Swiss
 */
 
/**
 *  Do some nice scripting here in your own way
 */
(function(Frontend) {
    /**
     * @type {string}
     */
    let publicString = "I'm coming from a public function";

    /**
     * @type {string}
     */
    let privateString = "I'm private";

    /**
     * This is how a private function looks like
     * @returns {string}
     */
    function getPrivateString() {
        return privateString;
    }

    /**
     * This is how a public function looks like
     * @returns {string}
     */
    Frontend.getPublicString = function() {
        return publicString;
    };
}(
    window.Frontend = window.Frontend || {}
));
