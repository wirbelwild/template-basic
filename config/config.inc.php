<?php return array (
  'pages' => 
  array (
    'de' =>
    array (
      0 => 
      array (
        'name' => '/',
        'pageTitle' => 'Welcome to Kiwa WCMS',
        'metaTags' => 
        array (
          'og:title' => 'Welcome to Kiwa WCMS',
          'og:description' => 'A feather light website content management system for developers.',
          'description' => 'A feather light website content management system for developers.',
          'og:image' => '',
          'og:type' => 'website',
        ),
        'getFile' => 'index',
        'enableKeywordSearch' => false,
        'hasDynamicChild' => false,
        'enableCache' => false,
        'childOf' => false,
      ),
    ),
  ),
  'urlStructure' => 
  array (
    0 => 'name',
    1 => 'subname',
  ),
); ?>