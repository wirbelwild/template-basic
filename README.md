[![PHP 7.1](https://img.shields.io/badge/php-7.1-green.svg)](http://www.php.net)
[![Codacy Badge](https://api.codacy.com/project/badge/Grade/033574ffd09a47299e58e1f64eeaa42c)](https://www.codacy.com/app/bitandblack/template-basic?utm_source=wirbelwild@bitbucket.org&amp;utm_medium=referral&amp;utm_content=wirbelwild/template-basic&amp;utm_campaign=Badge_Grade)
[![Latest Stable Version](https://poser.pugx.org/kiwa/template-basic/v/stable)](https://packagist.org/packages/kiwa/template-basic)
[![Total Downloads](https://poser.pugx.org/kiwa/template-basic/downloads)](https://packagist.org/packages/kiwa/template-basic)
[![Latest Unstable Version](https://poser.pugx.org/kiwa/template-basic/v/unstable)](https://packagist.org/packages/kiwa/template-basic)
[![License](https://poser.pugx.org/kiwa/template-basic/license)](https://packagist.org/packages/kiwa/template-basic)
[![composer.lock](https://poser.pugx.org/kiwa/template-basic/composerlock)](https://packagist.org/packages/kiwa/template-basic)

# Basic Template for Kiwa WCMS 

This is the basic template for Kiwa WCMS. 
It gives a welcome screen in your website's root as long as you didn't write your own website template.

## Installation

You don't need to install this template, because it's already inside your Kiwa WCMS installation. 
Anyhow: This template is no stand-alone application: You need to install Kiwa WCMS at first. 
This template belongs to the `template` folder like every other template.

## Create your own template 

This one is also a demo template. If you want to create your own template, please have a look at all the file in here and also at our [template skeleton](https://packagist.org/packages/kiwa/template-skeleton). 
It creates all the folders you need and makes use of the [template console](https://packagist.org/packages/kiwa/template-console) to define database connections, create pages and add meta tags.

## Help 

If you have questions targeting the template development, contact us under `kiwa@bitandblack.com`.
