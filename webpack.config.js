const Encore = require("@symfony/webpack-encore");
const CopyWebpackPlugin = require("copy-webpack-plugin");
const GoogleFontsPlugin = require("@beyonk/google-fonts-webpack-plugin");
const isProduction = Encore.isProduction();
const publicPath = isProduction 
    ? "/build" 
    : "/build"
;

Encore
    .setOutputPath("build/")
    .setPublicPath(publicPath)
    .setManifestKeyPrefix("build/")
    .addEntry("template", "./assets/javascripts/template.js")
    .cleanupOutputBeforeBuild()
    .enableBuildNotifications()
    .enableSourceMaps(!isProduction)
    .enableVersioning(isProduction)
    .enableSassLoader()
    .enablePostCssLoader()
    .disableSingleRuntimeChunk()
    .autoProvidejQuery()
    .addPlugin(new CopyWebpackPlugin([
        {
            from: "./assets/images", 
            to: "images"
        }
    ]))
    .addPlugin(new GoogleFontsPlugin({
        path: "fonts",
        fonts: [
            {
                family: "Source Code Pro",
                variants: [
                    "400",
                ]
            },
            {
                family: "Source Sans Pro",
                variants: [
                    "400",
                    "400italic",
                    "700"
                ]
            }
        ]
    }))
    .configureBabel((babelConfig) => {
        const preset = babelConfig.presets.find(([name]) => name === "@babel/preset-env");
        if (preset !== undefined) {
            preset[1].useBuiltIns = "usage";
            preset[1].corejs = "3.0.0";
        }
    })
;

module.exports = Encore.getWebpackConfig();